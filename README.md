# Pixtris

Exercise to recreate a proof-of-concept Tetris clone.

> Unity Version 2021.1.25f1

Ryan Blakeman ©2020

rblakeman31@gmail.com

https://ryanblakeman.com
