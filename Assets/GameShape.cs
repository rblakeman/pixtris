using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum SHAPE {
    NONE, S, Z, SQUARE, LONG, L, J, T, ERROR
};

public enum ROT {
    _0, _90, _180, _270
};

public class GameShape {
    public List<int[]> coords;
    public SHAPE shapetype;
    public Color color;
    public ROT rotationIdx; // 0,1,2,3 (0, 90, 180, 270)

    public GameShape(SHAPE _shapetype, List<int[]> _coords, ROT _rot = ROT._0) {
        this.coords = _coords;
        this.shapetype = _shapetype;
        this.color = this.setColor(_shapetype);
        this.rotationIdx = _rot;
    }

    public GameShape(SHAPE _shapetype, int[] tile1, int[] tile2, int[] tile3, int[] tile4, ROT _rot = ROT._0) {
        this.coords = new List<int[]> {
            tile1, tile2, tile3, tile4
        };

        this.shapetype = _shapetype;
        this.color = this.setColor(_shapetype);
        this.rotationIdx = _rot;
    }

    private Color setColor(SHAPE shape) {
        switch (shape) {
            case SHAPE.NONE:
                Debug.LogError("Error GameTile.cs setColor SHAPE.NONE");
                // Empty space
                return Color.white;
            case SHAPE.S:
                return Color.blue;
            case SHAPE.Z:
                return Color.red;
            case SHAPE.SQUARE:
                return Color.yellow;
            case SHAPE.LONG:
                return Color.green;
            case SHAPE.L:
                return Color.magenta;
            case SHAPE.J:
                return Color.cyan;
            case SHAPE.T:
                return Color.gray;
            default:
                // error
                Debug.LogError("Error GameTile.cs setColor");
                return Color.black;
        }
    }
}
