﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameTile {
    public GameObject tile;
    public SHAPE shapetype;
    public Color color;

    public GameTile(SHAPE _shapetype, GameObject _tile) {
        this.tile = _tile;

        this.shapetype = _shapetype;
        this.color = this.setColor(_shapetype);
    }

    public GameTile(GameTile _gameTile) {
        this.tile = _gameTile.tile;

        this.shapetype = _gameTile.shapetype;
        this.color = _gameTile.color;
    }

    private Color setColor(SHAPE shape) {
        switch (shape) {
            case SHAPE.NONE:
                // Empty space
                return Color.white;
            case SHAPE.S:
                return Color.blue;
            case SHAPE.Z:
                return Color.red;
            case SHAPE.SQUARE:
                return Color.yellow;
            case SHAPE.LONG:
                return Color.green;
            case SHAPE.L:
                return Color.magenta;
            case SHAPE.J:
                return Color.cyan;
            case SHAPE.T:
                return Color.gray;
            default:
                // error
                Debug.LogError("Error GameTile.cs setColor");
                return Color.black;
        }
    }
}
