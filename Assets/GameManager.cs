﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using static GameTile;

public class GameManager : MonoBehaviour {
    // Grid dimensions
    public static int height = 20;
    public static int width = 10;

    // List of upcoming shapes (just their id)
    public List<SHAPE> deck = new List<SHAPE>();
    // List of shapes landed on grid
    public List<GameShape> landedShapes = new List<GameShape>();
    // X,Y grid coords, colored by value of pixel/tile
    public GameTile[,] grid = new GameTile[width, height];

    // Needed for transform
    public GameObject gameBoard;
    // Needed to Instantiate new shape
    public GameObject spritePrefab;

    // List of coordinates for new shape
    public List<int[]> fallingShapeCoords = null;
    public SHAPE fallingShapeType = SHAPE.NONE;
    public ROT fallingShapeRotation = ROT._0;
    public bool isFalling = false;
    public float nextActionTime = 0.0f;
    public float heartbeatInterval = 24.0f;

    void printGrid() {
        string line = "";
        for (int y = 0; y < height; y++) {
            for (int x = 0; x < width; x++) {
                line += grid[x,y].shapetype.ToString();
            }
            line += "\n";
        }
        Debug.Log(line);
    }

    /*
    * Render grid of tiles on update
    */
    void renderGrid() {
        for (int y = 0; y < height; y++) {
            for (int x = 0; x < width; x++) {
                grid[x,y].tile.GetComponent<SpriteRenderer>().color = grid[x,y].color;
            }
        }
    }

    SHAPE getShape(int value, string errorMsg) {
        switch (value) {
            case 1:
                return SHAPE.S;
            case 2:
                return SHAPE.Z;
            case 3:
                return SHAPE.SQUARE;
            case 4:
                return SHAPE.LONG;
            case 5:
                return SHAPE.L;
            case 6:
                return SHAPE.J;
            case 7:
                return SHAPE.T;
            default:
                // error
                Debug.LogError(errorMsg);
                return SHAPE.ERROR;
        }
    }

    /*
    * Fill in queue of shapes on start
    */
    void fillDeck() {
        for (int i = 0; i < 5; i++) {
            int newValue = Random.Range(1,8);
            // // Avoid immediate duplciates
            // if (i != 0) {
            //     while (deck[i-1] == newValue) {
            //         newValue = Random.Range(1,8);
            //     }
            // }
            deck.Add(getShape(newValue, "Error GameManager.cs fillDeck"));
        }
    }

    void printDeck() {
        string line = "";
        foreach (int shape in deck) {
            line += shape.ToString();
            line += " ";
        }
        Debug.Log(line);
    }

    /*
    * Shift deck forward and add new shape to end
    */
    void addToDeck() {
        for (int i = 0; i < 4; i++) {
            deck[i] = deck[i+1];
        }

        int newValue = Random.Range(1,8);
        // // Avoid immediate duplciates
        // while (deck[3] == newValue) {
        //     newValue = Random.Range(1,8);
        // }

        deck[4] = getShape(newValue, "Error GameManager.cs fillDeck");
    }

    private List<int[]> getShapePos(SHAPE shape) {
        switch(shape) {
            case SHAPE.S:
                return new List<int[]> {
                    new int[] {2,0}, // --OO  // --21
                    new int[] {1,0}, // -OO-  // -43-
                    new int[] {1,1}, // ----  // ----
                    new int[] {0,1}  // ----  // ----
                };
            case SHAPE.Z:
                return new List<int[]> {
                    new int[] {0,0}, // -OO-  // -12-
                    new int[] {1,0}, // --OO  // --34
                    new int[] {1,1}, // ----  // ----
                    new int[] {2,1}  // ----  // ----
                };
            case SHAPE.SQUARE:
                return new List<int[]> {
                    new int[] {0,0}, // -OO-  // -12-
                    new int[] {1,0}, // -OO-  // -34-
                    new int[] {0,1}, // ----  // ----
                    new int[] {1,1}  // ----  // ----
                };
            case SHAPE.LONG:
                return new List<int[]> {
                    new int[] {0,0}, // -O--  // -1--
                    new int[] {0,1}, // -O--  // -2--
                    new int[] {0,2}, // -O--  // -3--
                    new int[] {0,3}  // -O--  // -4--
                };
            case SHAPE.L:
                return new List<int[]> {
                    new int[] {0,0}, // -O--  // -1--
                    new int[] {0,1}, // -O--  // -2--
                    new int[] {0,2}, // -OO-  // -34-
                    new int[] {1,2}  // ----  // ----
                };
            case SHAPE.J:
                return new List<int[]> {
                    new int[] {1,0}, // --O-  // --1-
                    new int[] {1,1}, // --O-  // --2-
                    new int[] {1,2}, // -OO-  // -43-
                    new int[] {0,2}  // ----  // ----
                };
            case SHAPE.T:
                return new List<int[]> {
                    new int[] {0,0}, // -OOO  // -123
                    new int[] {1,0}, // --O-  // --4-
                    new int[] {2,0}, // ----  // ----
                    new int[] {1,1}  // ----  // ----
                };
            default:
                Debug.LogError("error getShape else");
                return null;
        }
    }

    private Color getShapeColor(SHAPE shape) {
        switch(shape) {
            case SHAPE.NONE:
                // Empty space
                return Color.white;
            case SHAPE.S:
                return Color.blue;
            case SHAPE.Z:
                return Color.red;
            case SHAPE.SQUARE:
                return Color.yellow;
            case SHAPE.LONG:
                return Color.green;
            case SHAPE.L:
                return Color.magenta;
            case SHAPE.J:
                return Color.cyan;
            case SHAPE.T:
                return Color.gray;
            default:
                // error
                Debug.LogError("Error GameTile.cs setColor");
                return Color.black;
        }
    }

    /*
    * Add new shape to top of board
    */
    void dropNewShape() {
        isFalling = true;

        int startingPosX = 4;//4 or 5
        int startingPosY = 0;

        fallingShapeCoords = getShapePos(deck[0]);
        fallingShapeType = deck[0];
        fallingShapeRotation = ROT._0;

        landedShapes.Add(new GameShape(fallingShapeType, fallingShapeCoords));

        for (int i = 0; i < fallingShapeCoords.Count; i++) {
            int newPosX = startingPosX + fallingShapeCoords[i][0];
            int newPosY = startingPosY + fallingShapeCoords[i][1];
            grid[newPosX, newPosY].shapetype = deck[0];
            grid[newPosX, newPosY].color = getShapeColor(deck[0]);
            fallingShapeCoords[i] = new int[] {newPosX, newPosY};
        }
    }

    void gravity() {
        /* 
        -- old approach, maybe helpful if bugs come up --

        // List<int[]> copy = grid.ToList();

        // check for collision below
        foreach(int[] tile in fallingShapeCoords) {
            // Debug.Log(tile[0]);
            // Debug.Log(tile[1]+1);
            // Debug.Log(grid[tile[0], tile[1]+1].value);
            Debug.Log("--------");
            if (tile[1] + 1 == height) {
                // hit floor
                isFalling = false;
            } else if (grid[tile[0], tile[1] + 1].shapetype != SHAPE.NONE) {
                // check for piece
                bool isSelf = false;
                foreach(int[] tile2 in fallingShapeCoords) {
                    // dont collide with self
                    if (tile[0] == tile2[0] && tile[1] + 1 == tile2[1]) {
                        isSelf = true;
                    }
                }
                if (!isSelf) {
                    isFalling = false;
                }
            }
        }
        */
        bool isFallPossible = true;

        if (isFalling) {
                // REMEMBER:
                // fallingShapeCoords[i][0] == x pos
                // fallingShapeCoords[i][1] == y pos

            // reset the grid at all shape pos
            // must do this first so you don't accidentally collide with yourself
            for (int i = 0; i < fallingShapeCoords.Count; i++) {
                grid[fallingShapeCoords[i][0], fallingShapeCoords[i][1]].shapetype = SHAPE.NONE;
                grid[fallingShapeCoords[i][0], fallingShapeCoords[i][1]].color = getShapeColor(SHAPE.NONE);
            }

            // check if falling is possible
            for (int i = 0; i < fallingShapeCoords.Count; i++) {
                if (
                    // Reached bottom of the board
                    fallingShapeCoords[i][1] + 1 >= height ||
                    // a different shape is in the way
                    grid[fallingShapeCoords[i][0], fallingShapeCoords[i][1] + 1].shapetype != SHAPE.NONE
                ) {
                    Debug.Log("fall NOT Possible");
                    isFallPossible = false;
                    break;
                }
            }

            if (isFallPossible) {
                // MOVE BLOCK DOWN ONE
                for (int i = 0; i < fallingShapeCoords.Count; i++) {
                    grid[fallingShapeCoords[i][0], fallingShapeCoords[i][1] + 1].shapetype = fallingShapeType;
                    grid[fallingShapeCoords[i][0], fallingShapeCoords[i][1] + 1].color = getShapeColor(fallingShapeType);

                    // update fallingShapeCoords state with new pos
                    fallingShapeCoords[i] = new int[] {fallingShapeCoords[i][0], fallingShapeCoords[i][1]+1};
                }
            } else {
                // UNDO FIRST FOR LOOP, MOVE WAS NOT POSSIBLE
                for (int i = 0; i < fallingShapeCoords.Count; i++) {
                    grid[fallingShapeCoords[i][0], fallingShapeCoords[i][1]].shapetype = fallingShapeType;
                    grid[fallingShapeCoords[i][0], fallingShapeCoords[i][1]].color = getShapeColor(fallingShapeType);
                }
            }

            if (!isFallPossible) {
                isFalling = false;
            }
        } else {
            fallingShapeCoords = null;
            fallingShapeType = SHAPE.NONE;
        }
    }

    void moveLeft() {
            // REMEMBER:
            // fallingShapeCoords[i][0] == x pos
            // fallingShapeCoords[i][1] == y pos
        bool isMovePossible = true;

        // reset the grid at all shape pos
        // must do this first so you don't accidentally collide with yourself
        for (int i = 0; i < fallingShapeCoords.Count; i++) {
            grid[fallingShapeCoords[i][0], fallingShapeCoords[i][1]].shapetype = SHAPE.NONE;
            grid[fallingShapeCoords[i][0], fallingShapeCoords[i][1]].color = getShapeColor(SHAPE.NONE);
        }

        // check if moving is possible
        for (int i = 0; i < fallingShapeCoords.Count; i++) {
            if (
                // Reached left-side of the board
                fallingShapeCoords[i][0] - 1 < 0 ||
                // a different shape is in the way
                grid[fallingShapeCoords[i][0] - 1, fallingShapeCoords[i][1]].shapetype != SHAPE.NONE
            ) {
                Debug.Log("move NOT Possible");
                isMovePossible = false;
                break;
            }
        }

        if (isMovePossible) {
            // MOVE BLOCK LEFT ONE
            for (int i = 0; i < fallingShapeCoords.Count; i++) {
                grid[fallingShapeCoords[i][0] - 1, fallingShapeCoords[i][1]].shapetype = fallingShapeType;
                grid[fallingShapeCoords[i][0] - 1, fallingShapeCoords[i][1]].color = getShapeColor(fallingShapeType);

                // update fallingShapeCoords state with new pos
                fallingShapeCoords[i] = new int[] {fallingShapeCoords[i][0] - 1, fallingShapeCoords[i][1]};
            }
        } else {
            // UNDO FIRST FOR LOOP, MOVE WAS NOT POSSIBLE
            for (int i = 0; i < fallingShapeCoords.Count; i++) {
                grid[fallingShapeCoords[i][0], fallingShapeCoords[i][1]].shapetype = fallingShapeType;
                grid[fallingShapeCoords[i][0], fallingShapeCoords[i][1]].color = getShapeColor(fallingShapeType);
            }
        }
    }

    void moveRight() {
            // REMEMBER:
            // fallingShapeCoords[i][0] == x pos
            // fallingShapeCoords[i][1] == y pos
        bool isMovePossible = true;

        // reset the grid at all shape pos
        // must do this first so you don't accidentally collide with yourself
        for (int i = 0; i < fallingShapeCoords.Count; i++) {
            grid[fallingShapeCoords[i][0], fallingShapeCoords[i][1]].shapetype = SHAPE.NONE;
            grid[fallingShapeCoords[i][0], fallingShapeCoords[i][1]].color = getShapeColor(SHAPE.NONE);
        }

        // check if moving is possible
        for (int i = 0; i < fallingShapeCoords.Count; i++) {
            if (
                // Reached right-side of the board
                fallingShapeCoords[i][0] + 1 > width - 1 ||
                // a different shape is in the way
                grid[fallingShapeCoords[i][0] + 1, fallingShapeCoords[i][1]].shapetype != SHAPE.NONE
            ) {
                Debug.Log("move NOT Possible");
                isMovePossible = false;
                break;
            }
        }

        if (isMovePossible) {
            // MOVE BLOCK LEFT ONE
            for (int i = 0; i < fallingShapeCoords.Count; i++) {
                grid[fallingShapeCoords[i][0] + 1, fallingShapeCoords[i][1]].shapetype = fallingShapeType;
                grid[fallingShapeCoords[i][0] + 1, fallingShapeCoords[i][1]].color = getShapeColor(fallingShapeType);

                // update fallingShapeCoords state with new pos
                fallingShapeCoords[i] = new int[] {fallingShapeCoords[i][0] + 1, fallingShapeCoords[i][1]};
            }
        } else {
            // UNDO FIRST FOR LOOP, MOVE WAS NOT POSSIBLE
            for (int i = 0; i < fallingShapeCoords.Count; i++) {
                grid[fallingShapeCoords[i][0], fallingShapeCoords[i][1]].shapetype = fallingShapeType;
                grid[fallingShapeCoords[i][0], fallingShapeCoords[i][1]].color = getShapeColor(fallingShapeType);
            }
        }
    }

    // Clockwise turns
    void rotate() {
            // REMEMBER:
            // fallingShapeCoords[i][0] == x pos
            // fallingShapeCoords[i][1] == y pos
        bool isRotatePossible = true;
        List<int[]> tileOffsets = new List<int[]>{
            new int[] {0,0}, //tile0 x,y
            new int[] {0,0}, //tile1 x,y
            new int[] {0,0}, //tile2 x,y
            new int[] {0,0}  //tile3 x,y
        };

        // reset the grid at all shape pos
        // must do this first so you don't accidentally collide with yourself
        for (int i = 0; i < fallingShapeCoords.Count; i++) {
            grid[fallingShapeCoords[i][0], fallingShapeCoords[i][1]].shapetype = SHAPE.NONE;
            grid[fallingShapeCoords[i][0], fallingShapeCoords[i][1]].color = getShapeColor(SHAPE.NONE);
        }

        // get the rotation offsets
        switch(fallingShapeType) {
            case SHAPE.S:
                // the existing rotation (not the new rotation)
                switch(fallingShapeRotation) {
                    case ROT._0:
                        // --10      // -3--
                        // -32- ---> // -21-
                        // ----      // --0-
                        // ----      // ----
                        tileOffsets[0] = new int[] {-1,2};
                        tileOffsets[1] = new int[] {0,1};
                        tileOffsets[2] = new int[] {-1,0};
                        tileOffsets[3] = new int[] {0,-1};
                        break;
                    case ROT._90:
                        // -3--      // --23
                        // -21- ---> // -01-
                        // --0-      // ----
                        // ----      // ----
                        tileOffsets[0] = new int[] {-1,-1};
                        tileOffsets[1] = new int[] {0,0};
                        tileOffsets[2] = new int[] {1,-1};
                        tileOffsets[3] = new int[] {2,0};
                        break;
                    case ROT._180:
                        // --23      // -0--
                        // -01- ---> // -12-
                        // ----      // --3-
                        // ----      // ----
                        tileOffsets[0] = new int[] {0,-1};
                        tileOffsets[1] = new int[] {-1,0};
                        tileOffsets[2] = new int[] {0,1};
                        tileOffsets[3] = new int[] {-1,2};
                        break;
                    case ROT._270:
                        // -0--      // --10
                        // -12- ---> // -32-
                        // --3-      // ----
                        // ----      // ----
                        tileOffsets[0] = new int[] {2,0};
                        tileOffsets[1] = new int[] {1,-1};
                        tileOffsets[2] = new int[] {0,0};
                        tileOffsets[3] = new int[] {-1,-1};
                        break;
                }
                break;
            case SHAPE.Z:

                break;
            case SHAPE.SQUARE:

                break;
            case SHAPE.LONG:

                break;
            case SHAPE.L:

                break;
            case SHAPE.J:

                break;
            case SHAPE.T:

                break;
            default:
                isRotatePossible = false;
                break;
        }

        // check if rotating is possible
        for (int i = 0; i < fallingShapeCoords.Count; i++) {
            if (
                // Reached top of the board
                fallingShapeCoords[i][1] + tileOffsets[i][1] < 0 ||
                // Reached bottom of the board
                fallingShapeCoords[i][1] + tileOffsets[i][1] >= height ||
                // Reached left-side of the board
                fallingShapeCoords[i][0] + tileOffsets[i][0] < 0 ||
                // Reached right-side of the board
                fallingShapeCoords[i][0] + tileOffsets[i][0] > width - 1 ||
                // a different shape is in the way
                grid[fallingShapeCoords[i][0] + tileOffsets[i][0], fallingShapeCoords[i][1] + tileOffsets[i][1]].shapetype != SHAPE.NONE
            ) {
                Debug.Log("move NOT Possible");
                isRotatePossible = false;
                break;
            }
        }

        if (isRotatePossible) {
            // ROTATE BLOCK CLOCKWISE ONCE
            for (int i = 0; i < fallingShapeCoords.Count; i++) {
                grid[fallingShapeCoords[i][0] + tileOffsets[i][0], fallingShapeCoords[i][1] + tileOffsets[i][1]].shapetype = fallingShapeType;
                grid[fallingShapeCoords[i][0] + tileOffsets[i][0], fallingShapeCoords[i][1] + tileOffsets[i][1]].color = getShapeColor(fallingShapeType);

                // update fallingShapeCoords state with new pos
                fallingShapeCoords[i] = new int[] {fallingShapeCoords[i][0] + tileOffsets[i][0], fallingShapeCoords[i][1] + tileOffsets[i][1]};
            }
            // Increment rot idx
            switch(fallingShapeRotation) {
                case ROT._0:
                    fallingShapeRotation = ROT._90;
                    break;
                case ROT._90:
                    fallingShapeRotation = ROT._180;
                    break;
                case ROT._180:
                    fallingShapeRotation = ROT._270;
                    break;
                case ROT._270:
                    fallingShapeRotation = ROT._0;
                    break;
            }
        } else {
            // UNDO FIRST FOR LOOP, ROTATE WAS NOT POSSIBLE
            for (int i = 0; i < fallingShapeCoords.Count; i++) {
                grid[fallingShapeCoords[i][0], fallingShapeCoords[i][1]].shapetype = fallingShapeType;
                grid[fallingShapeCoords[i][0], fallingShapeCoords[i][1]].color = getShapeColor(fallingShapeType);
            }
        }
    }

    void Awake() {
        // Create Empty Grid
        for (int y = 0; y < height; y++) {
            for (int x = 0; x < width; x++) {
                string gameObjectName = x.ToString()+"-"+y.ToString();
                GameObject gameTile = Instantiate(spritePrefab);
                gameTile.name = gameObjectName;
                gameTile.transform.position = new Vector3((float)x, -(float)y, 0f);
                gameTile.transform.parent = gameBoard.transform;
                gameTile.tag = "GameTile";
                gameTile.AddComponent<SpriteRenderer>();

                grid[x,y] = new GameTile(
                    SHAPE.NONE,
                    gameTile
                );
            }
        }
        fillDeck();
    }

    void Start() {
        // printGrid();
        printDeck();
    }

    void Update() {
        bool heartbeat = false;
        if (Time.time > nextActionTime) {
            nextActionTime += heartbeatInterval;
            heartbeat = true;
        }

        // LEFT
        if (Input.GetKeyUp("a") && isFalling) {
            moveLeft();
        // RIGHT
        } else if (Input.GetKeyUp("d") && isFalling) {
            moveRight();
        // DOWN
        } else if (Input.GetKeyUp("s") && isFalling) {
            gravity();
        // ROTATE
        } else if (Input.GetKeyUp("w") && isFalling) {
            rotate();
        }

        // GRAVITY
        if (heartbeat && isFalling) {
            gravity();
        }
        // SPAWN
        else if (heartbeat && !isFalling) {
            dropNewShape();
            addToDeck();
            printDeck();
        }

        renderGrid();
    }
}
